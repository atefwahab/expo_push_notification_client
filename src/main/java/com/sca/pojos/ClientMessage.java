package com.sca.pojos;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Feature;

public class ClientMessage {

	@JsonFormat(with = Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	List<String> to;
	String title;
	String message;
	Map<String, Object> data;
	public ClientMessage() {

	}

	
	
	
	
	
	
	






	/**
	 * @return the to
	 */
	public List<String> getTo() {
		return to;
	}














	/**
	 * @param to the to to set
	 */
	public void setTo(List<String> to) {
		this.to = to;
	}














	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}














	public Map<String, Object> getData() {
		return data;
	}














	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	
	
	
	
	
	
}
