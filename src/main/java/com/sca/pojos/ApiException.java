package com.sca.pojos;

import java.time.ZonedDateTime;

public class ApiException {

	private String message;
	private String code;
	private ZonedDateTime zonedDateTime;
	
	
	public ApiException(String message, String code) {
		super();
		this.message = message;
		this.code = code;
		this.zonedDateTime = ZonedDateTime.now();
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public ZonedDateTime getZonedDateTime() {
		return zonedDateTime;
	}


	public void setZonedDateTime(ZonedDateTime zonedDateTime) {
		this.zonedDateTime = zonedDateTime;
	}
	
	
	
	

}
