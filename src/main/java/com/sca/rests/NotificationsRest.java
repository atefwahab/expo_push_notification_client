package com.sca.rests;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sca.exceptions.ApiRequestException;
import com.sca.pojos.ClientMessage;
import com.sca.services.ExpoService;

import io.github.jav.exposerversdk.ExpoPushReceipt;
import io.github.jav.exposerversdk.PushClientException;
import io.github.jav.exposerversdk.PushNotificationException;

@RestController
@RequestMapping("/notifications/api")
public class NotificationsRest {
	
	@Autowired
	ExpoService expoService;

	@GetMapping("/sayHello")
	public String sayHello() {
		return "HELLO REST";
	}
	
	
	@PostMapping("send")
	public List<ExpoPushReceipt> sendNotification(@RequestHeader(name = "authorization",required = false) String accessToken ,@RequestBody ClientMessage clientMessage) throws Exception {

		
			try {
			 List<ExpoPushReceipt> reciepts =expoService.sendNotification(clientMessage.getTo(), clientMessage.getTitle(),clientMessage.getMessage(), clientMessage.getData(), accessToken);
			 
			 return reciepts;
			
			}
			catch (PushClientException | InterruptedException e) {
				if(e instanceof ApiRequestException) {
					throw e;
				}else {
					e.printStackTrace();
				}
					
				
			}
			
			return null;
		
		
	}
}
