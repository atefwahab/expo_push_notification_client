package com.sca.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.sca.exceptions.ApiRequestException;

import io.github.jav.exposerversdk.ExpoPushMessage;
import io.github.jav.exposerversdk.ExpoPushMessageTicketPair;
import io.github.jav.exposerversdk.ExpoPushReceipt;
import io.github.jav.exposerversdk.ExpoPushTicket;
import io.github.jav.exposerversdk.PushClient;
import io.github.jav.exposerversdk.PushClientException;
import io.github.jav.exposerversdk.PushNotificationException;

@Service
public class ExpoService {

	public List<ExpoPushReceipt> sendNotification(List<String> recipients, String title, String message,
			Map<String, Object> data, String accessToken)
			throws PushClientException, InterruptedException, PushNotificationException, ApiRequestException, ExecutionException {

		for (String recipient : recipients) {
			// if invalid token tell user
			if (!PushClient.isExponentPushToken(recipient)) {
				throw new Error("Token:" + recipient + " is not a valid token.");
			}

		}

		ExpoPushMessage expoPushMessage = new ExpoPushMessage();
		expoPushMessage.addAllTo(recipients);
		expoPushMessage.setTitle(title);
		expoPushMessage.setBody(message);
		if (data != null) {
			expoPushMessage.setData(data);
		}

		List<ExpoPushMessage> expoPushMessages = new ArrayList<>();
		expoPushMessages.add(expoPushMessage);

		PushClient client = new PushClient();

		List<List<ExpoPushMessage>> chunks = client.chunkPushNotifications(expoPushMessages);

		List<CompletableFuture<List<ExpoPushTicket>>> messageRepliesFutures = new ArrayList<>();

		// send notification and wait for it
		for (List<ExpoPushMessage> chunk : chunks) {
			messageRepliesFutures.add(client.sendPushNotificationsAsync(accessToken, chunk));
		}

		// Wait for each completable future to finish
		List<ExpoPushTicket> allTickets = new ArrayList<>();
		for (CompletableFuture<List<ExpoPushTicket>> messageReplyFuture : messageRepliesFutures) {
			try {
				for (ExpoPushTicket ticket : messageReplyFuture.get()) {
					allTickets.add(ticket);
				}

			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				throw e;
//				e.printStackTrace();
			}
		}

		List<ExpoPushMessageTicketPair<ExpoPushMessage>> zippedMessagesTickets = client
				.zipMessagesTickets(expoPushMessages, allTickets);

		List<ExpoPushMessageTicketPair<ExpoPushMessage>> okTicketMessages = client
				.filterAllSuccessfulMessages(zippedMessagesTickets);
		String okTicketMessagesString = okTicketMessages.stream().map(p -> "Title: " + p.message.getTitle() + ", Id:"
				+ p.ticket.getId() + ", token:" + p.ticket.getAdditionalProperties()).collect(Collectors.joining(","));
		System.out
				.println("Recieved OK ticket for " + okTicketMessages.size() + " messages: " + okTicketMessagesString);

		List<ExpoPushMessageTicketPair<ExpoPushMessage>> errorTicketMessages = client
				.filterAllMessagesWithError(zippedMessagesTickets);
		String errorTicketMessagesString = errorTicketMessages.stream()
				.map(p -> "Title: " + p.message.getTitle() + ", Error: " + p.ticket.getDetails().getError())
				.collect(Collectors.joining(","));
		System.out.println(
				"Recieved ERROR ticket for " + errorTicketMessages.size() + " messages: " + errorTicketMessagesString);

		// Countdown 30s
//         int wait = recipients.size();
//         for (int i = wait; i >= 0; i--) {
//             System.out.print("Waiting for " + wait + " seconds. " + i + "s\r");
//             Thread.sleep(1000);
//         }
		System.out.println("Fetching reciepts...");

//         List<String> ticketIds = (client.getTicketIdsFromPairs(okTicketMessages));
		List<String> ticketIds = (client.getTicketIdsFromPairs(zippedMessagesTickets));
		CompletableFuture<List<ExpoPushReceipt>> receiptFutures = client.getPushNotificationReceiptsAsync(accessToken,
				ticketIds);

		List<ExpoPushReceipt> receipts = new ArrayList<>();
		try {
			receipts = receiptFutures.get();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("Recieved " + receipts.size() + " receipts:");

		for (ExpoPushReceipt reciept : receipts) {
			System.out.println("Receipt for id: " + reciept.getId() + " had status: " + reciept.getStatus());

		}

		return receipts;

	}

}
