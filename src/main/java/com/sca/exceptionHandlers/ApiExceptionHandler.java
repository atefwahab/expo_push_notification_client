package com.sca.exceptionHandlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.sca.exceptions.ApiRequestException;
import com.sca.pojos.ApiException;

@ControllerAdvice
public class ApiExceptionHandler {
	
	
	private static final Logger log = LoggerFactory.getLogger(ApiExceptionHandler.class);

	
	@ExceptionHandler(value = {ApiRequestException.class})
	public ResponseEntity<ApiException> handleApiException(ApiRequestException apiRequestException){
		
		log.error(apiRequestException.getMessage(),apiRequestException);
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
		// Create Pojo 
		ApiException apiException = new ApiException(apiRequestException.getMessage(), httpStatus.toString());
		return new ResponseEntity<ApiException>(apiException, httpStatus);
		
	}


}
