package com.sca.exceptions;

public class ApiRequestException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String code;


	public ApiRequestException(String code,String message) {
		super(message);
		this.code = code;
	}

	public ApiRequestException(Throwable cause) {
		super(cause);
	}

	public ApiRequestException(String message, Throwable cause) {
		super(message, cause);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	
	

	

}
