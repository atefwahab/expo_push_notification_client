package io.github.jav.exposerversdk;

import java.net.URL;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import com.sca.exceptions.ApiRequestException;

public class PushClient extends PushClientCustomData<ExpoPushMessage> {

    public PushClient() throws PushClientException {
        super();
    }

    public URL getBaseApiUrl() {
        return baseApiUrl;
    }

    public PushClient setBaseApiUrl(URL _baseApiUrl) {
        baseApiUrl = _baseApiUrl;
        return this;
    }

    @Override
    public CompletableFuture<List<ExpoPushTicket>> sendPushNotificationsAsync(String accessToken,List<ExpoPushMessage> messages) throws PushNotificationException, ApiRequestException {
        return super.sendPushNotificationsAsync(accessToken,messages);
    }

    @Override
    public List<List<ExpoPushMessage>> chunkPushNotifications(List<ExpoPushMessage> messages) {
        return super.chunkPushNotifications(messages);
    }
}
